//
//  Schedule.swift
//  ScheduleProject
//
//  Created by mac charles on 09/01/2023.
//

import Foundation

//
//  Activity.swift
//  ScheduleProject
//
//  Created by mac charles on 09/01/2023.
//



import Foundation


struct Schedule:Identifiable, Codable {
    var id:Int
    var createdTime : Date
    var fields: Fields
    
    struct Fields: Codable{
        var end: Date
        var activity:String
        var type:String
        var speaker : Speaker
        var start: Date
        var location: String
        var notes: String
        
        
        struct Speaker: Codable{
            var name: [String]
        }
        
    }
    
}


    
   
