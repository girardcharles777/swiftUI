//
//  Network.swift
//  ScheduleProject
//
//  Created by mac charles on 09/01/2023.
//

import Foundation
import SwiftUI


class Network: ObservableObject {
        @Published var schedules: [Schedule] = []
    
    func getSchedules() {
        print("on test la fction")
        guard let url = URL(string: "https://api.airtable.com/v0/appLxCaCuYWnjaSKB/%F0%9F%93%86%20Schedule/recNQeClhQH7oRI1G") else { fatalError("Missing URL") }

        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = 100
        urlRequest.httpMethod = "GET"
        let accessToken = "keymaCPSexfxC2hF9"
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField:
         "Authorization")
        


        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }

            if response.statusCode == 200 {
                print("on récupère des trucs code : 200")
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let decodedUsers = try JSONDecoder().decode([Schedule].self, from: data)
                        self.schedules = decodedUsers
                    } catch let error {
                        print("Error decoding: ", error)
                    }
                }
            }
        }

        dataTask.resume()
    }
}
