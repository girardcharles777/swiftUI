//
//  EventApp.swift
//  ScheduleProject
//
//  Created by mac charles on 09/01/2023.
//

import SwiftUI

struct EventApp: View {
    //@State private var selectedDay: Day?
    //@EnvironmentObject var network: Network
    @ObservedObject var network = Network()

        
       /* let event = Event(name: "Festival de la communication", days: [
            Day(id:1,date: Date(), activities: [
                Activity(id:1,title: "Conférence sur les réseaux sociaux", location: "Salle 1", startTime: Date(), endTime: Date(), description: "Conférence sur les réseaux sociaux et leur impact sur les entreprises."),
                Activity(id:2, title: "Atelier de photographie", location: "Salle 2", startTime: Date(), endTime: Date(), description: "Atelier de photographie pour apprendre les bases de la prise de vue.")
            ]),
            Day(id:2, date: Date().addingTimeInterval(86400), activities: [
                Activity(id: 3, title: "Atelier de marketing numérique", location: "Salle 3", startTime: Date(), endTime: Date(), description: "Atelier sur les stratégies de marketing numérique pour les entreprises.")
            ])
        ])*/


            var body: some View {
                
                NavigationView {
                    
                    // Affiche la liste de liens de navigation vers chaque jour de l'événement
                    ForEach(network.schedules) { schedule in
                    
                        NavigationLink(destination: DetailsEvent(schedule: schedule)) {
                            Text(schedule.fields.activity)
                        }
                        
                    }
                    .onAppear(perform: {
                        network.getSchedules()
                    })
                }
            }
    
        
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EventApp()
            .environmentObject(Network())
    }
}


