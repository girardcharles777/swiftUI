//
//  dayView.swift
//  ScheduleProject
//
//  Created by mac charles on 09/01/2023.
//

import SwiftUI

struct DetailsEvent: View {
    let schedule: Schedule

    var body: some View {
        VStack(alignment: .leading) {
            ForEach(schedule.fields) { activity in
                HStack(alignment: .center){
                    Text(activity.description)
                    Text("Debut : ", format: activity.date)
                    Text("fin : ", format: activity.end)
                    Text(activity.type)
                    Text(activity.location)

                }
                
            }
        }
        
    }
}
      
    



