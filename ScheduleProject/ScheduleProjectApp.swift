//
//  ScheduleProjectApp.swift
//  ScheduleProject
//
//  Created by mac charles on 08/01/2023.
//

import SwiftUI
import Foundation



@main
struct ScheduleProjectApp: App {
    var network = Network()
    
    var body: some Scene {
        WindowGroup {
            EventApp()
                .environmentObject(network)
        }
    }
        
    }







